def filterFIR(data, cutoff_hz, sampleFreq=25, plotFilter=False):
	from scipy.signal import kaiserord, lfilter, firwin, freqz, filtfilt
	from pylab import figure, plot, title, grid, xlabel, ylabel, ylim, xlim
	import numpy as np

	nyq_rate = sampleFreq/2.0
	width = 5/nyq_rate
	ripple_db = 60.0

	N, beta = kaiserord(ripple_db, width)

	taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta), pass_zero=False)
	
	if plotFilter:
		figure(figsize=(16,9))
		plot(taps, 'bo-')
		title('Filter Coefficients (%d taps)'%N)
		grid(True)

		figure(figsize=(16,9))
		w, h = freqz(taps, worN=80000)
		plot((w/np.pi)*nyq_rate, np.absolute(h))
		xlabel('Frequency [Hz]')
		ylabel('Gain')
		title('Filter Frequency Response')
		ylim(-0.05, 1.05)
		xlim(0, 5)
		grid(True)
	
	#return lfilter(taps, 1, data)
	return filtfilt(taps, np.ones(1), data)

def doEEMD(data, noisestd=-1, nensemble=100, imfs=12):
	import EMD as emdplain
	import numpy as np

	if noisestd == -1:
		noisestd = 0.1*np.std(data)

	data_emd = emdplain.emd(data, nimfs=imfs)
	print data_emd.shape[1]

	for j in range(0,nensemble-1):
		data_noisy = data + np.random.normal(0,noisestd,len(data))
		data_emd = data_emd + emdplain.emd(data_noisy, nimfs=data_emd.shape[1])
		
	data_emd = data_emd/nensemble



	return data_emd

def smooth(x,window_len=11,window='hanning'):
    import numpy
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=numpy.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=numpy.ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')

    y=numpy.convolve(w/w.sum(),s,mode='valid')
    return y